//
//  ViewController.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "ViewController.h"
#import "CBLBottomBackgroundView.h"
#import "CBLThemeColorLayer.h"
#import "UIView+InputControl.h"

@interface ViewController ()


@property (nonatomic, strong) CBLBottomBackgroundView   *bottomBackgroundView;

@property (nonatomic, strong) CBLBottomStartView        *startView;//立即用车

@property (nonatomic, strong) CBLBottomLockNumberView   *lockNumberView;//输入车牌号

@property (nonatomic, strong) CBLBottomLockCodeView     *lockCodeView;//显示车锁密码

@property (nonatomic, strong) CBLBottomRideInfoView     *rideInfoView;//骑行信息

@property (nonatomic, strong) CBLBottomPayInfoView      *payInfoView;//支付信息

@end

@implementation ViewController

#pragma mark - Life Cycle
#pragma mark - Init
-(instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)initialize{
    self.view.backgroundColor = [UIColor colorWithRed:0.514 green:0.663 blue:0.890 alpha:1.000];
    
    
    [self.view setupKeyboardShow:^(NSNotification *notification) {
        self.view.y -= 216;
    } hide:^(NSNotification *notification) {
        self.view.y = 0;
    }];
    [self.view setupForDismissKeyboard];
}

#pragma mark - Life Cycle
-(void)viewDidLoad{
    [super viewDidLoad];
    [self initialize];
    
    //Add Subview
    [self.view addSubview:self.bottomBackgroundView];
    
    //Add Action
    
    //------------init frams or constraints of subviews------
    [self setupFrameAndConstraints];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //TODO data update
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}



#pragma mark - DataSource Control


#pragma mark - View's Display and Behavior
-(void)setupFrameAndConstraints{
    self.bottomBackgroundView.frame = CGRectMake(0, SCREEN_HEIGHT - kBottomeBackgroundHeight, KBottomeBackgroundWidth, kBottomeBackgroundHeight);
}

#pragma mark mark - Event Response Action
//Method like Button click,gestureRecognizer



#pragma mark - public methods


#pragma mark - private methods
- (void)switchToStartView {
    [self.bottomBackgroundView switchToNextView:self.startView];
}

- (void)switchToLockNumberView {
    [self.bottomBackgroundView switchToNextView:self.lockNumberView];
    [self.lockNumberView.lockNumberInputFiled becomeFirstResponder];
}

- (void)switchToLockCodeView {
    [self.bottomBackgroundView switchToNextView:self.lockCodeView];
    [self.lockCodeView beginCountTime];
}

- (void)switchToRideInfoView {
    [self.bottomBackgroundView switchToNextView:self.rideInfoView];
    [self.rideInfoView beginRecordRideInfo];
}

- (void)switchToPayInfoView {
    [self.rideInfoView finishRide];
    [self.bottomBackgroundView switchToNextView:self.payInfoView];
}

#pragma mark - System Delegate
//Delegate method like TableViewDeleagte


#pragma mark -Custom Delegate


#pragma mark - getters and Setters

-(UIView *)bottomBackgroundView {
    if(!_bottomBackgroundView){
        _bottomBackgroundView = [[CBLBottomBackgroundView alloc] initWithFirstView:self.startView];
        _bottomBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomBackgroundView;
}

- (CBLBottomStartView *)startView{
    if(!_startView){
        _startView = [CBLBottomStartView new];
        [_startView.startButton addTarget:self action:@selector(switchToLockNumberView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _startView;
}//立即用车

- (CBLBottomLockNumberView *)lockNumberView{
    if(!_lockNumberView){
        _lockNumberView = [CBLBottomLockNumberView new];
        [_lockNumberView.getLockCodeButton addTarget:self action:@selector(switchToLockCodeView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _lockNumberView;
}//输入车牌号

- (CBLBottomLockCodeView *)lockCodeView {
    if(!_lockCodeView){
        //初始化
        _lockCodeView = [CBLBottomLockCodeView new];
        __weak typeof(self)weakSelf = self;
        [_lockCodeView setOnTimeFinished:^{
            [weakSelf switchToRideInfoView];
        }];
    }
    return _lockCodeView;
}//显示车锁密码

- (CBLBottomRideInfoView *)rideInfoView {
    if(!_rideInfoView){
        //初始化
        _rideInfoView = [CBLBottomRideInfoView new];
        [_rideInfoView.finishRideButton addTarget:self action:@selector(switchToPayInfoView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rideInfoView;
}//骑行信息

- (CBLBottomPayInfoView *)payInfoView {
    if(!_payInfoView){
        //初始化
        _payInfoView = [CBLBottomPayInfoView new];
        [_payInfoView.payCostButton addTarget:self action:@selector(switchToStartView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _payInfoView;
}//支付信息


@end
