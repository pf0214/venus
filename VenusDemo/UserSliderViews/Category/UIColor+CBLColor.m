//
//  UIColor+CBLColor.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "UIColor+CBLColor.h"

@implementation UIColor (CBLColor)

+ (UIColor *)lightThemeColor {
    return [UIColor colorWithRed:1.000 green:0.925 blue:0.000 alpha:1.000];
}

+ (UIColor *)darkThemeColor {
    return [UIColor colorWithRed:0.996 green:0.800 blue:0.000 alpha:1.000];
}


@end
