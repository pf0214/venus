//
//  UIView+CoreGraphic.h
//  VenusDemo
//
//  Created by pf on 19/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CoreGraphic)

//绘制渐变填充路径
- (void)drawLinearGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor;

@end
