//
//  UIView+InputControl.h
//  VenusDemo
//
//  Created by pf on 20/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (InputControl)

-(void)setupKeyboardShow:(void(^)(NSNotification *notification))showblock hide:(void (^)(NSNotification *notification))hideblock;

-(void)setupForDismissKeyboard;

@end
