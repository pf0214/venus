//
//  UIView+CBLFrame.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "UIView+CBLFrameAndShape.h"

@implementation UIView (CBLFrameAndShape)

-(CGFloat)x{
    return self.frame.origin.x;
}

-(CGFloat)y{
    return self.frame.origin.y;
}

-(CGFloat)width{
    return self.size.width;
}

-(CGFloat)height{
    return self.size.height;
}

-(CGSize)size{
    return self.bounds.size;
}

-(CGPoint)origin{
    return self.frame.origin;
}

-(void)setX:(CGFloat)x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

-(void)setY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

-(void)setWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

-(void)setHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

-(void)setSize:(CGSize)size{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

-(void)setOrigin:(CGPoint)origin{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}


-(CGFloat)radius{
    return self.layer.cornerRadius;
}

-(void)setRadius:(CGFloat)radius{
    if (radius < 0) {
        radius = 0;
    }
    //圆角半径
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}

@end
