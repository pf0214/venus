//
//  UIView+InputControl.m
//  VenusDemo
//
//  Created by pf on 20/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "UIView+InputControl.h"

@implementation UIView (InputControl)

-(void)setupKeyboardShow:(void(^)(NSNotification *notifacation))showblock hide:(void (^)(NSNotification *notifacation))hideblock {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    NSOperationQueue *mainQuene = [NSOperationQueue mainQueue];
    [notificationCenter addObserverForName:UIKeyboardWillShowNotification object:nil queue:mainQuene usingBlock:showblock];
    
    [notificationCenter addObserverForName:UIKeyboardWillHideNotification object:nil queue:mainQuene usingBlock:hideblock];
    
    //Setting tap hid gesture when the keyboard is showed and remove the tap gesture when it disappear
    [self setupForDismissKeyboard];
}

-(void)setupForDismissKeyboard{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHidAction)];
    
    __weak typeof(self)weakSelf = self;
    NSOperationQueue *mainQuene = [NSOperationQueue mainQueue];
    [notificationCenter addObserverForName:UIKeyboardWillShowNotification object:nil queue:mainQuene usingBlock:^(NSNotification * _Nonnull note) {
        [weakSelf addGestureRecognizer:singleTap];
    }];
    
    [notificationCenter addObserverForName:UIKeyboardWillHideNotification object:nil queue:mainQuene usingBlock:^(NSNotification * _Nonnull note) {
        [weakSelf removeGestureRecognizer:singleTap];
    }];
}


-(void)tapHidAction{
    [self endEditing:YES];
}


@end
