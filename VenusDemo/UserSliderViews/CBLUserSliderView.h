  //
//  CBLUserSliderView.h
//  VenusDemo
//
//  Created by pf on 25/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CBLUserSliderViewDelegate <NSObject>

- (void)didSelectItemAtIdex:(NSInteger)index;

@end

@interface CBLUserSliderView : UIView



@end
