//
//  CBLUserSliderView.m
//  VenusDemo
//
//  Created by pf on 25/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLUserSliderView.h"
#import "CBLSliderTopShapeLayer.h"
#import "CBLSliderBottomShapeLayer.h"
#import "CBLSliderItemButton.h"


@interface CBLUserSliderView ()

@property (nonatomic, strong) CBLSliderTopShapeLayer    *topShapeLayer;

@property (nonatomic, strong) CBLSliderBottomShapeLayer *bottomShapeLayer;

@property (nonatomic, strong) NSMutableArray            *selectItems;

@property (nonatomic, strong) UIImageView               *userImageView;

@property (nonatomic, strong) UILabel                   *userNameLabel;

@property (nonatomic, strong) UIImageView               *userTypeIcon;
@property (nonatomic, strong) UILabel                   *userTypeLabel;//用户类型

@end

@implementation CBLUserSliderView

#pragma mark - Initialize

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}


-(void)initialize{
    //init subviews and data
    
    //----setup subviews frame and constraints
    [self setupFrameAndConstraints];
}

#pragma mark - Public Methods


#pragma mark - Private Methods
#pragma mark Views Behavior

-(void)setupFrameAndConstraints{
    
}

#pragma mark DataSource Control


#pragma mark Event Action


#pragma mark - Delegate
#pragma mark System Delegate


#pragma mark Custom Delegate


#pragma mark - Getters And Setters



@end
