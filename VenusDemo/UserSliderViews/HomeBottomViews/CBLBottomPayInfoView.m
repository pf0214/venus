//
//  CBLBottomPayInfoView.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomPayInfoView.h"

#define kFinishButtonWidth (SCREEN_WIDTH * 0.75)
#define kFinishButtonHeight 40

#define kCostLabelBoardLeft 30
#define kCostLabelHeight 25

@interface CBLBottomPayInfoView ()

@property (nonatomic, strong) UILabel *payInfoDesLabel;//骑行费用描述

@property (nonatomic, strong) UILabel *payFinalCostLabel;//骑行最终费用

@property (nonatomic, strong) UILabel *payTotalCostLabel;//骑行总费用

@property (nonatomic, strong) UILabel *payDiscountCostLabel;//骑行优惠劵费用

@property (nonatomic, strong) UILabel *paySecurityLabel;//骑行安全协议

@end

@implementation CBLBottomPayInfoView


#pragma mark - Initialize

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}


-(void)initialize{
    //init subviews and data
    
    //----setup subviews frame and constraints
    [self setupFrameAndConstraints];
}


#pragma mark - DataSource Control


#pragma mark - Event Action


#pragma mark - Views Behavior

-(void)setupFrameAndConstraints{
    
}


#pragma mark - Public Methods


#pragma mark - Private Methods
- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self addSubview:self.payInfoDesLabel];
    [self addSubview:self.payFinalCostLabel];
    [self addSubview:self.payTotalCostLabel];
    [self addSubview:self.payDiscountCostLabel];
    [self addSubview:self.payCostButton];
    [self addSubview:self.paySecurityLabel];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.payInfoDesLabel.frame = CGRectMake(0, 20, self.width, 20);
    
    self.payFinalCostLabel.frame = CGRectMake(0,
                                          CGRectGetMaxY(self.payInfoDesLabel.frame),
                                          self.width,
                                          70);
    
    self.payTotalCostLabel.frame = CGRectMake(kCostLabelBoardLeft ,
                                          CGRectGetMaxY(self.payFinalCostLabel.frame) + 15,
                                          self.width / 2.0 - kCostLabelBoardLeft,
                                          kCostLabelHeight);
    
    self.payDiscountCostLabel.frame = CGRectMake(self.width / 2.0,
                                              self.payTotalCostLabel.y,
                                              self.payTotalCostLabel.width,
                                              self.payTotalCostLabel.height);
    
    self.payCostButton.frame = CGRectMake((self.width - kFinishButtonWidth) / 2.0,
                                             CGRectGetMaxY(self.payTotalCostLabel.frame) + 20,
                                             kFinishButtonWidth,
                                             kFinishButtonHeight);
    
    self.payCostButton.themeColorLayer.cornerRadius = kFinishButtonHeight / 2.0;
    
    self.paySecurityLabel.frame = CGRectMake(0 , self.height - 35, self.width, kCostLabelHeight);
}

#pragma mark - System Delegate


#pragma mark - Custom Delegate


#pragma mark - Getters And Setters

- (UILabel *)payInfoDesLabel {
    if(!_payInfoDesLabel){
        _payInfoDesLabel = [UILabel new];
        _payInfoDesLabel.text = NSLocalizedString(@"本次行程消费（元）", nil);
        _payInfoDesLabel.textColor = [UIColor lightGrayColor];
        _payInfoDesLabel.textAlignment = NSTextAlignmentCenter;
        _payInfoDesLabel.font = [UIFont systemFontOfSize:13];
    }
    return _payInfoDesLabel;
}//骑行时间描述

- (UILabel *)payFinalCostLabel {
    if(!_payFinalCostLabel){
        _payFinalCostLabel = [UILabel new];
        _payFinalCostLabel.text = NSLocalizedString(@"0.0", nil);
        _payFinalCostLabel.textColor = [UIColor blackColor];
        _payFinalCostLabel.textAlignment = NSTextAlignmentCenter;
        _payFinalCostLabel.font = [UIFont fontWithName:@"DINAlternate-Bold" size:60];
    }
    return _payFinalCostLabel;
}//骑行时间

- (UILabel *)payTotalCostLabel {
    if(!_payTotalCostLabel){
        _payTotalCostLabel = [UILabel new];
        _payTotalCostLabel.text = NSLocalizedString(@"行程扣费：2.0元", nil);
        _payTotalCostLabel.textColor = [UIColor lightGrayColor];
        _payTotalCostLabel.textAlignment = NSTextAlignmentCenter;
        _payTotalCostLabel.font = [UIFont systemFontOfSize:12];
    }
    return _payTotalCostLabel;
}//骑行费用

- (UILabel *)payDiscountCostLabel {
    if(!_payDiscountCostLabel){
        _payDiscountCostLabel = [UILabel new];
        _payDiscountCostLabel.text = NSLocalizedString(@"优惠劵：-1.13元", nil);
        _payDiscountCostLabel.textColor = [UIColor lightGrayColor];
        _payDiscountCostLabel.textAlignment = NSTextAlignmentCenter;
        _payDiscountCostLabel.font = [UIFont systemFontOfSize:12];
    }
    return _payDiscountCostLabel;
}//骑行距离

- (CBLThemeColorButton *)payCostButton {
    if(!_payCostButton){
        _payCostButton = [CBLThemeColorButton new];
        _payCostButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_payCostButton setTitle:@"确认支付" forState:UIControlStateNormal];
        [_payCostButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _payCostButton;
}//结束骑行

- (UILabel *)paySecurityLabel {
    if(!_paySecurityLabel){
        _paySecurityLabel = [UILabel new];
        _paySecurityLabel.text = NSLocalizedString(@"ofo已经为您本次出行购买了《安全保障》", nil);
        _paySecurityLabel.textColor = [UIColor lightGrayColor];
        _paySecurityLabel.textAlignment = NSTextAlignmentCenter;
        _paySecurityLabel.font = [UIFont systemFontOfSize:12];
    }
    return _paySecurityLabel;
}//骑行安全协议
@end


