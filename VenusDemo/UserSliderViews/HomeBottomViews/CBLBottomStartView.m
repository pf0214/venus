//
//  CBLStartBottomView.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomStartView.h"
#import <Masonry/Masonry.h>
#import "CBLThemeColorLayer.h"

#define kStartButtonWidth (SCREEN_WIDTH * 0.37)

#define kUserButtonWidth 22
#define kUserButtonBoardLeft 20

@interface CBLBottomBaseView ()

@end

@implementation CBLBottomStartView

#pragma mark - Initialize

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.startButton];
    [self addSubview:self.userButton];
    [self addSubview:self.giftButton];
}

#pragma mark - DataSource Control


#pragma mark - Event Action


#pragma mark - Views Behavior


#pragma mark - Public Methods


#pragma mark - Private Methods
-(void)layoutSubviews {
    [super layoutSubviews];
    self.startButton.frame = CGRectMake((self.width - kStartButtonWidth) / 2.0,
                                        (self.height - kStartButtonWidth) / 2.0,
                                        kStartButtonWidth,
                                        kStartButtonWidth);
    self.startButton.themeColorLayer.cornerRadius = self.startButton.width / 2.0;
    
    self.userButton.frame = CGRectMake(25,
                                       self.height - kUserButtonBoardLeft - kUserButtonWidth,
                                       kUserButtonWidth,
                                       kUserButtonWidth);
    
    self.giftButton.frame = CGRectMake(self.width - kUserButtonBoardLeft - kUserButtonWidth,
                                       self.height - kUserButtonBoardLeft - kUserButtonWidth,
                                       kUserButtonWidth,
                                       kUserButtonWidth);
}

#pragma mark - System Delegate


#pragma mark - Custom Delegate


#pragma mark - Getters And Setters
-(CBLThemeColorButton *)startButton {
    if(!_startButton){
        //初始化
        _startButton = [CBLThemeColorButton new];
        _startButton.titleLabel.numberOfLines = 0;
        _startButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        NSString *title = @"GO\n立即用车";
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc]initWithString:title];
        [attributeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"DINAlternate-Bold" size:47]} range:NSMakeRange(0, 2)];
        [attributeString setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:15]} range:NSMakeRange(2, 5)];
        [_startButton setAttributedTitle:attributeString forState:UIControlStateNormal];
        [_startButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _startButton;
}

-(UIButton *)userButton {
    if(!_userButton){
        _userButton = [UIButton new];
        [_userButton setImage:[UIImage imageNamed:@"user"] forState:UIControlStateNormal];
    }
    return _userButton;
}

-(UIButton *)giftButton {
    if(!_giftButton){
        _giftButton = [UIButton new];
        [_giftButton setImage:[UIImage imageNamed:@"gift"] forState:UIControlStateNormal];
    }
    return _giftButton;
}


@end
