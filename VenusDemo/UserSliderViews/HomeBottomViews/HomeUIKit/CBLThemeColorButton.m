//
//  CBLThemColorView.m
//  VenusDemo
//
//  Created by pf on 19/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLThemeColorButton.h"
#import "CBLThemeColorLayer.h"


@implementation CBLThemeColorButton

-(void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    self.themeColorLayer.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
}

- (CAGradientLayer *)themeColorLayer {
    if(!_themeColorLayer){
        _themeColorLayer = [CBLThemeColorLayer horizonLayer];
        [self.layer insertSublayer:_themeColorLayer atIndex:0];
    }
    return _themeColorLayer;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.themeColorLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

@end
