//
//  CBLLockCodeView.h
//  VenusDemo
//
//  Created by pf on 18/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBLLockCodeDisplayView : UIView

@property (nonatomic, strong) NSString *lockCode;

@end
