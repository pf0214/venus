//
//  CBLThemeColorLayer.h
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CBLThemeColorLayer : CAGradientLayer

+ (instancetype)verticalLayer;

+ (instancetype)horizonLayer;

@end
