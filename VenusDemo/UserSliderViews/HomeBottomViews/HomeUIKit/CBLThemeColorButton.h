//
//  CBLThemColorView.h
//  VenusDemo
//
//  Created by pf on 19/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBLThemeColorButton : UIButton

@property (nonatomic, strong) CAGradientLayer *themeColorLayer;

@end
