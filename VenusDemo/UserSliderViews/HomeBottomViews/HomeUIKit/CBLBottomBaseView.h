//
//  CBLBottomBaseView.h
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

#import "UIView+CBLFrameAndShape.h"
#import "UIView+CBLAnimation.h"


#define SCREEN_WIDTH ([UIScreen  mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

@interface CBLBottomBaseView : UIView

- (instancetype)initWithFrame:(CGRect)frame ViewModel:(id)viewModel;

@end
