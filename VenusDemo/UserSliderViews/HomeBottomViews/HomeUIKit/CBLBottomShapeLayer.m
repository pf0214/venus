//
//  CBLBottomShapeLayer.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomShapeLayer.h"

@interface CBLBottomShapeLayer ()


@end

@implementation CBLBottomShapeLayer

-(void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.arcHeight = 40;
}

- (void)setArcHeight:(NSInteger)arcHeight {
    _arcHeight = arcHeight;
    [self setNeedsDisplay];
}

-(void)drawInContext:(CGContextRef)ctx {
    UIGraphicsPushContext(ctx);
    
    CGContextSetFillColorWithColor(ctx, self.fillColor);
    //抗锯齿
    CGContextSetAllowsAntialiasing(ctx, true);
    CGContextSetShouldAntialias(ctx, true);
    
    CGContextSaveGState(ctx);
    
    NSInteger contextWidth = self.bounds.size.width;
    NSInteger comtextHeight = self.bounds.size.height;

    CGMutablePathRef mutablePath = CGPathCreateMutable();
    
    CGPathMoveToPoint(mutablePath, NULL, 0, self.arcHeight);
    //左侧竖线、底部横线、右侧竖线
    CGPathAddLineToPoint(mutablePath, NULL, 0, comtextHeight);
    CGPathAddLineToPoint(mutablePath, NULL, contextWidth, comtextHeight);
    CGPathAddLineToPoint(mutablePath, NULL, contextWidth, self.arcHeight);
    
    CGPoint controlPoint = CGPointMake(contextWidth / 2.0, -self.arcHeight);
    CGPathAddQuadCurveToPoint(mutablePath, NULL, controlPoint.x, controlPoint.y, 0, self.arcHeight);
    
    CGPathAddLineToPoint(mutablePath, NULL, self.bounds.size.width, 0);
    
    CGPathCloseSubpath(mutablePath);
    
    CGContextAddPath(ctx, mutablePath);
    
    
    CGContextFillPath(ctx);
    
    CGContextRestoreGState(ctx);
    
    CGPathRelease(mutablePath);
    
}

@end
