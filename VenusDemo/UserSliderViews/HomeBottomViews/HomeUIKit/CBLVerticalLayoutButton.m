//
//  CBLVerticalLayoutButton.m
//  VenusDemo
//
//  Created by pf on 18/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLVerticalLayoutButton.h"



@implementation CBLVerticalLayoutButton

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.titleLabel.font = [UIFont systemFontOfSize:12];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    NSInteger width = self.frame.size.width;
    NSInteger height = self.frame.size.height;
    
    NSInteger imageWidth = height * 0.7 * 0.7;
    NSInteger imageBoard = (width - imageWidth) / 2;
    
    self.imageView.frame = CGRectMake(imageBoard ,imageBoard / 2, imageWidth, imageWidth);
    self.titleLabel.frame = CGRectMake(0, height * 0.7 , width, height * 0.3);
}

@end
