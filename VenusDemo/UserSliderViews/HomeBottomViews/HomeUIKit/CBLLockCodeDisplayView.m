//
//  CBLLockCodeView.m
//  VenusDemo
//
//  Created by pf on 18/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLLockCodeDisplayView.h"
#import <QuartzCore/QuartzCore.h>
#import "CBLThemeColorLayer.h"
#import "UIView+CoreGraphic.h"


#define kMaxCardWidth 60
#define kCardHeight (kCardWidth * 1.25)

#define kCardBoardTop 10
#define kCardBoardLeft 10
#define kCardHorizonMargin 3
#define kCardRadius 4

@interface CBLLockCodeDisplayView ()

@property (nonatomic, strong) NSDictionary *textAttributeDic;
@property (nonatomic, assign) NSInteger     charactorLength;
@property (nonatomic, assign) NSInteger     cardWidth;
@property (nonatomic, assign) NSInteger     cardHeight;

@property (nonatomic, assign) CGPoint       beginPoint;

@property (nonatomic, assign) CGSize        textSize;
@property (nonatomic, assign) NSInteger     textBoardLeft;
@property (nonatomic, assign) NSInteger     textBoardTop;

@end

@implementation CBLLockCodeDisplayView

#pragma mark - Initialize

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}


-(void)initialize{
    //init subviews and data
    self.backgroundColor = [UIColor clearColor];
}


#pragma mark - Public Methods
- (void)setOnTimeFinished:(void(^)())onTimeFinish {
    self.onTimeFinished = onTimeFinish;
}

#pragma mark - Private Methods


#pragma mark DataSource Control


#pragma mark Event Action


#pragma mark Views Behavior

#pragma mark - Delegate
#pragma mark System Delegate


#pragma mark Custom Delegate



#pragma mark - Getters And Setters

- (void)setLockCode:(NSString *)lockCode {
    _lockCode = lockCode;
    self.charactorLength = lockCode.length;
    [self setNeedsDisplay];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    if (self.charactorLength > 0) {
        NSInteger cardWidth = (frame.size.width - kCardBoardLeft * 2 - kCardHorizonMargin * (self.charactorLength - 1)) / self.charactorLength;
        self.cardWidth =  cardWidth > kMaxCardWidth ? kMaxCardWidth : cardWidth;
        self.cardHeight = self.cardWidth * 1.25;
        
        //文字区域宽度
        NSInteger areaWidth = self.charactorLength * (self.cardWidth + kCardHorizonMargin) - kCardHorizonMargin;
        //使文字区域能居中的起始点
        NSInteger startX = (frame.size.width - areaWidth) / 2.0;
        NSInteger startY = (frame.size.height - self.cardHeight) / 2.0;
        self.beginPoint = CGPointMake(startX, startY);
        
        //使文字在色块中居中
        self.textSize = [@"1" sizeWithAttributes:self.textAttributeDic];
        if (self.textSize.height > self.cardHeight) {
            self.textAttributeDic = @{NSFontAttributeName:[UIFont fontWithName:@"DINAlternate-Bold" size:12]};
            self.textSize = [@"1" sizeWithAttributes:self.textAttributeDic];
        }
        self.textBoardLeft = (self.cardWidth - self.textSize.width) / 2.0;
        self.textBoardTop = (self.cardHeight - self.textSize.height) / 2.0;
        [self setNeedsDisplay];
    }
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (int i = 0; i < self.lockCode.length; i++) {
        CGRect drawRect = CGRectMake(self.beginPoint.x + i * (self.cardWidth + kCardHorizonMargin),
                                     self.beginPoint.y,
                                     self.cardWidth,
                                     self.cardHeight);
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:drawRect cornerRadius:kCardRadius];
        UIColor *beginColor = [UIColor colorWithRed:1.000 green:0.925 blue:0.000 alpha:1.000];
        UIColor *endColor = [UIColor colorWithRed:0.996 green:0.800 blue:0.000 alpha:1.000];
        [self drawLinearGradient:context path:path.CGPath startColor:beginColor.CGColor endColor:endColor.CGColor];
        
        CGPoint drawPoint = CGPointMake(drawRect.origin.x + self.textBoardLeft,
                                        drawRect.origin.y + self.textBoardTop);
        
        NSString *charactor = [self.lockCode substringWithRange:NSMakeRange(i, 1)];
        [charactor drawInRect:CGRectMake(drawPoint.x, drawPoint.y, self.cardWidth, self.cardHeight) withAttributes:[self textAttributeDic]];
    }
}

- (NSDictionary *)textAttributeDic {
    if(!_textAttributeDic){
        _textAttributeDic = @{NSFontAttributeName:[UIFont fontWithName:@"DINAlternate-Bold" size:50]};
    }
    return _textAttributeDic;
}

//- (CAShapeLayer *)

@end
