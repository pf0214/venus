//
//  CBLBottomBaseView.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomBaseView.h"
#import "CBLBottomShapeLayer.h"

@interface CBLBottomBaseView ()

@property (nonatomic, strong) CBLBottomShapeLayer *shapeLayer;

@end

@implementation CBLBottomBaseView

#pragma mark - Initialize

-(instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    return [self initWithFrame:frame ViewModel:nil];
}

-(instancetype)initWithFrame:(CGRect)frame ViewModel:(id)viewModel {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

#pragma mark - DataSource Control


#pragma mark - Event Action


#pragma mark - Views Behavior


#pragma mark - Public Methods


#pragma mark - Private Methods



#pragma mark - Getters And Setters

- (CBLBottomShapeLayer *)shapeLayer {
    if(!_shapeLayer){
        //初始化
        _shapeLayer = [[CBLBottomShapeLayer alloc] init];
        self.layer.mask = _shapeLayer;
    }
    return _shapeLayer;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];

    self.shapeLayer.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    [self setNeedsDisplay];
}

@end
