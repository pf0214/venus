//
//  CBLThemeColorLayer.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLThemeColorLayer.h"
#import <UIKit/UIColor.h>

@implementation CBLThemeColorLayer

-(instancetype)init {
    self = [super init];
    if (self) {
        CGColorRef startColor = [UIColor colorWithRed:1.000 green:0.925 blue:0.000 alpha:1.000].CGColor;
        CGColorRef endColor = [UIColor colorWithRed:0.996 green:0.800 blue:0.000 alpha:1.000].CGColor;
        self.colors = @[(__bridge id)startColor,(__bridge id)endColor];
        
        self.shadowRadius = 10;
        self.shadowOffset = CGSizeMake(0, 10);
        self.shadowOpacity = 0.6;
        self.shadowColor = [UIColor colorWithRed:1.000 green:0.867 blue:0.431 alpha:0.6].CGColor;
    }
    return self;
}

+ (instancetype)verticalLayer {
    CBLThemeColorLayer *layer = [[CBLThemeColorLayer alloc] init];
    layer.startPoint = CGPointMake(1, 0.5);
    layer.endPoint = CGPointMake(0, 0.5);
    return layer;
}

+ (instancetype)horizonLayer {
    CBLThemeColorLayer *layer = [[CBLThemeColorLayer alloc] init];
    layer.startPoint = CGPointMake(0.0, 0.5);
    layer.endPoint = CGPointMake(1.0, 0.5);

    return layer;
}


@end
