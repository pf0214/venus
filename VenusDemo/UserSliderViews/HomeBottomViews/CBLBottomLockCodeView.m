//
//  CBLBottomLockCodeView.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomLockCodeView.h"
#import "CBLVerticalLayoutButton.h"
#import "CBLLockCodeDisplayView.h"

#define kTorchButtonWidth 50
const static NSInteger kCountTime = 10;

@interface CBLBottomLockCodeView()


@property (nonatomic, strong) UILabel                   *lockNumberDesLabel;//

@property (nonatomic, strong) UILabel                   *timeCountDesLabel;//

@property (nonatomic, strong) CBLLockCodeDisplayView    *lockCodeDisplayView;//

@property (nonatomic, strong) UIButton                  *torchBotton;

@property (nonatomic, strong) UIButton                  *helpButton;//

@property (nonatomic, strong) NSTimer           *timer;
@property (nonatomic, assign) NSTimeInterval    countTime;

@property (nonatomic, copy) void (^timeFinished)();

@end

@implementation CBLBottomLockCodeView
#pragma mark - Initialize

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)initialize{
    //init subviews and data
    
    //----setup subviews frame and constraints
    self.lockCodeDisplayView.lockCode = @"123456";
}

#pragma mark - DataSource Control


#pragma mark - Event Action


#pragma mark - Views Behavior
-(void)didMoveToSuperview {
    [self addSubview:self.lockNumberDesLabel];
    [self addSubview:self.timeCountDesLabel];
    [self addSubview:self.lockCodeDisplayView];
    [self addSubview:self.torchBotton];
    [self addSubview:self.helpButton];
    
}

- (void)layoutSubviews {
    self.lockNumberDesLabel.frame = CGRectMake(0, 30, SCREEN_WIDTH, 20);
    self.timeCountDesLabel.frame = CGRectMake(0, 70, SCREEN_WIDTH, 20);
    
    self.lockCodeDisplayView.frame = CGRectMake(0, 100, SCREEN_WIDTH, 80);
    
    self.torchBotton.frame = CGRectMake(SCREEN_WIDTH/2.0 - kTorchButtonWidth - 30,
                                        CGRectGetMaxY(self.lockCodeDisplayView.frame) + 10,
                                        kTorchButtonWidth,
                                        kTorchButtonWidth);
    
    self.helpButton.frame = CGRectMake(SCREEN_WIDTH / 2.0 + 30,
                                       self.torchBotton.y,
                                       kTorchButtonWidth,
                                       kTorchButtonWidth);
}

#pragma mark - Public Methods
- (void)beginCountTime {
    self.countTime = kCountTime;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(decreaseTime) userInfo:nil repeats:YES];
}

-(void)setOnTimeFinished:(void (^)())onTimeFinish {
    self.timeFinished = onTimeFinish;
}

#pragma mark - Private Methods

- (void)decreaseTime {
    self.countTime -= 1;
    self.timeCountDesLabel.attributedText = [self getCountTimeLabelAttributeStr:[NSString stringWithFormat:@"%lds",(long)self.countTime]];
    if (self.countTime < 0.001) {
        [self.timer invalidate];
        if (self.timeFinished) {
            self.timeFinished();
        }
    }
}

- (NSAttributedString *)getCountTimeLabelAttributeStr:(NSString *)times{
    NSDictionary *timeAttribute = @{NSFontAttributeName: [UIFont systemFontOfSize:13],NSForegroundColorAttributeName:[UIColor colorWithRed:0.925 green:0.380 blue:0.361 alpha:1.000]};
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:times attributes:timeAttribute];
    
    NSDictionary *desAttribute = @{NSFontAttributeName: [UIFont systemFontOfSize:13], NSForegroundColorAttributeName:[UIColor lightGrayColor]};
    [attribute appendAttributedString:[[NSAttributedString alloc]initWithString:@"后开始计费，如车辆有问题请报修" attributes:desAttribute]];
    return attribute;
}




#pragma mark - System Delegate


#pragma mark - Custom Delegate


#pragma mark - Getters And Setters
- (UILabel *)lockNumberDesLabel {
    if(!_lockNumberDesLabel){
        _lockNumberDesLabel = [UILabel new];
        _lockNumberDesLabel.text = NSLocalizedString(@"NO.12345678解锁码", nil);
        _lockNumberDesLabel.textColor = [UIColor blackColor];
        _lockNumberDesLabel.textAlignment = NSTextAlignmentCenter;
        _lockNumberDesLabel.font = [UIFont boldSystemFontOfSize:16];
    }
    return _lockNumberDesLabel;
}//车牌号显示区

- (UILabel *)timeCountDesLabel {
    if(!_timeCountDesLabel){
        _timeCountDesLabel = [UILabel new];
        _timeCountDesLabel.attributedText = [self getCountTimeLabelAttributeStr:[NSString stringWithFormat:@"%lds",kCountTime]];
        _timeCountDesLabel.textColor = [UIColor lightGrayColor];
        _timeCountDesLabel.textAlignment = NSTextAlignmentCenter;
        _timeCountDesLabel.font = [UIFont systemFontOfSize:13];
    }
    return _timeCountDesLabel;
}//倒计时显示区

- (CBLLockCodeDisplayView *)lockCodeDisplayView {
    if(!_lockCodeDisplayView){
        _lockCodeDisplayView = [CBLLockCodeDisplayView new];
        _lockCodeDisplayView.backgroundColor = [UIColor clearColor];
    }
    return _lockCodeDisplayView;
}//解锁码显示区

- (UIButton *)torchBotton{
    if(!_torchBotton){
        _torchBotton = [CBLVerticalLayoutButton new];
        _torchBotton.titleLabel.font = [UIFont systemFontOfSize:11];
        [_torchBotton setTitle:@"扫码解锁" forState:UIControlStateNormal];
        [_torchBotton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_torchBotton setImage:[UIImage imageNamed:@"gift"] forState:UIControlStateNormal];
    }
    return _torchBotton;
}//手电筒

- (UIButton *)helpButton{
    if(!_helpButton){
        _helpButton = [CBLVerticalLayoutButton new];
        _helpButton.titleLabel.font = [UIFont systemFontOfSize:11];
        [_helpButton setTitle:@"开锁帮助" forState:UIControlStateNormal];
        [_helpButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_helpButton setImage:[UIImage imageNamed:@"gift"] forState:UIControlStateNormal];
    }
    return _helpButton;
}//开锁帮助
@end
