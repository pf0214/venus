//
//  CBLLockNumberView.h
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomBaseView.h"
#import "CBLThemeColorButton.h"

@interface CBLBottomLockNumberView : CBLBottomBaseView

@property (nonatomic, strong) UITextField               *lockNumberInputFiled;

@property (nonatomic, strong) CBLThemeColorButton       *getLockCodeButton;

@property (nonatomic, strong) UIButton                  *torchButton;

@property (nonatomic, strong) UIButton                  *scanQRCodeButton;

@end

