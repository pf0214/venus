//
//  CBLBottomLockCodeView.h
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomBaseView.h"

@interface CBLBottomLockCodeView : CBLBottomBaseView

- (void)beginCountTime;
- (void)setOnTimeFinished:(void(^)())onTimeFinish;

@end
