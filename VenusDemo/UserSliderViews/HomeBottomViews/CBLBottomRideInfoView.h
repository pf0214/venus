//
//  CBLBottomRideInfoView.h
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomBaseView.h"
#import "CBLThemeColorButton.h"

@interface CBLBottomRideInfoView : CBLBottomBaseView

@property (nonatomic,strong) CBLThemeColorButton *finishRideButton;//结束骑行

- (void)beginRecordRideInfo;
- (void)finishRide;

@end
