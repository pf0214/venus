//
//  CBLBottomBackgroundView.h
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomBaseView.h"

#import "CBLBottomStartView.h"
#import "CBLBottomLockNumberView.h"
#import "CBLBottomLockCodeView.h"
#import "CBLBottomRideInfoView.h"
#import "CBLBottomPayInfoView.h"

#define KBottomeBackgroundWidth SCREEN_WIDTH
#define kBottomeBackgroundHeight (SCREEN_WIDTH * 0.7)

typedef NS_ENUM(NSUInteger, CBLBottomState) {
    CBLBottomStateStart,
    CBLBottomStateLockNum,
    CBLBottomStateLockCode,
    CBLBottomStateRidInfo,
    CBLBottomStatePayInfo
};

@interface CBLBottomBackgroundView : CBLBottomBaseView
@property (nonatomic, strong) UIView                    *lastView;

@property (nonatomic, strong) UIView                    *currentView;//当前View页面

@property (nonatomic, assign) CBLBottomState            *currentViewStatus;

- (instancetype)initWithFirstView:(CBLBottomBaseView *)firstView;

- (void)switchToNextView:(CBLBottomBaseView *)view;

- (void)switchToNextView:(CBLBottomBaseView *)view animation:(void (^)())animation;

@end
