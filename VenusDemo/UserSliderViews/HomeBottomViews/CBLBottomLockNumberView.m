//
//  CBLLockNumberView.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomLockNumberView.h"
#import "CBLVerticalLayoutButton.h"
#import "CBLThemeColorLayer.h"
#import "CBLThemeColorButton.h"

#define kInputFiledWidth (SCREEN_WIDTH * 0.8)
#define kInputFiledHeith 40

#define kBoraderLeft (SCREEN_WIDTH - kInputFiledWidth) / 2.0

#define kTorchButtonWidth (50)

@implementation CBLBottomLockNumberView

#pragma mark - Initialize

-(instancetype)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}


-(void)initialize{
    //init subviews and data
    [self addSubview:self.lockNumberInputFiled];
    [self addSubview:self.getLockCodeButton];
    [self addSubview:self.torchButton];
    [self addSubview:self.scanQRCodeButton];
}

#pragma mark - DataSource Control


#pragma mark - Event Action


#pragma mark - Views Behavior
- (void)layoutSubviews {
    [super layoutSubviews];
    self.lockNumberInputFiled.frame = CGRectMake(kBoraderLeft,
                                                 50,
                                                 kInputFiledWidth,
                                                 kInputFiledHeith);
    self.lockNumberInputFiled.radius = kInputFiledHeith / 2.0;
    
    
    self.getLockCodeButton.frame = CGRectMake(kBoraderLeft,
                                              CGRectGetMaxY(self.lockNumberInputFiled.frame) + 15,
                                              kInputFiledWidth,
                                              kInputFiledHeith);
    self.getLockCodeButton.themeColorLayer.cornerRadius = kInputFiledHeith / 2.0;
    
    self.torchButton.frame = CGRectMake(SCREEN_WIDTH / 2 - kTorchButtonWidth - 30,
                                        CGRectGetMaxY(self.getLockCodeButton.frame) + 30,
                                        kTorchButtonWidth,
                                        kTorchButtonWidth);
    
    self.scanQRCodeButton.frame = CGRectMake(SCREEN_WIDTH / 2 + 30,
                                             self.torchButton.y,
                                             kTorchButtonWidth,
                                             kTorchButtonWidth);
}

#pragma mark - Public Methods


#pragma mark - Private Methods


#pragma mark - System Delegate


#pragma mark - Custom Delegate


#pragma mark - Getters And Setters

- (UITextField *)lockNumberInputFiled {
    if(!_lockNumberInputFiled){
        _lockNumberInputFiled = [UITextField new];
        _lockNumberInputFiled.textColor = [UIColor blackColor];
        _lockNumberInputFiled.keyboardType = UIKeyboardTypeNumberPad;
        _lockNumberInputFiled.font = [UIFont fontWithName:@"DINAlternate-Bold" size:20];
        _lockNumberInputFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入车牌号" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}];
        
        _lockNumberInputFiled.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 0)];
        _lockNumberInputFiled.leftViewMode = UITextFieldViewModeAlways;
        _lockNumberInputFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
        _lockNumberInputFiled.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _lockNumberInputFiled.layer.borderWidth = 0.7;
    }
    return _lockNumberInputFiled;
}

- (CBLThemeColorButton *)getLockCodeButton {
    if(!_getLockCodeButton){
        _getLockCodeButton = [CBLThemeColorButton new];
        _getLockCodeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_getLockCodeButton setTitle:@"获取解锁码" forState:UIControlStateNormal];
        [_getLockCodeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _getLockCodeButton;
}

- (UIButton *)torchButton {
    if(!_torchButton){
        _torchButton = [CBLVerticalLayoutButton new];
        _torchButton.titleLabel.font = [UIFont systemFontOfSize:11];
        [_torchButton setTitle:@"手电筒" forState:UIControlStateNormal];
        [_torchButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_torchButton setImage:[UIImage imageNamed:@"gift"] forState:UIControlStateNormal];

    }
    return _torchButton;
}

- (UIButton *)scanQRCodeButton {
    if(!_scanQRCodeButton){
        _scanQRCodeButton = [CBLVerticalLayoutButton new];
        _scanQRCodeButton.titleLabel.font = [UIFont systemFontOfSize:11];
        [_scanQRCodeButton setTitle:@"扫码解锁" forState:UIControlStateNormal];
        [_scanQRCodeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_scanQRCodeButton setImage:[UIImage imageNamed:@"gift"] forState:UIControlStateNormal];
    }
    return _scanQRCodeButton;
}

@end
