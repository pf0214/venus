//
//  CBLBottomRideInfoView.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomRideInfoView.h"

#define kFinishButtonWidth (SCREEN_WIDTH * 0.75)
#define kFinishButtonHeight 40

#define kCostLabelBoardLeft 40
#define kCostLabelHeight 40

#define kSecurityLabelHeight 35

@interface CBLBottomRideInfoView ()

@property (nonatomic, strong) UILabel *rideTimeDesLabel;//骑行时间描述

@property (nonatomic, strong) UILabel *rideTimeLabel;//骑行时间

@property (nonatomic, strong) UILabel *rideCostLabel;//骑行费用

@property (nonatomic, strong) UILabel *rideDistanceLabel;//骑行距离

@property (nonatomic, strong) UILabel *rideSecurityLabel;//骑行安全协议


@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, assign) NSTimeInterval rideTime;

@end

@implementation CBLBottomRideInfoView

#pragma mark - Initialize
-(instancetype)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void)initialize{
    //init subviews and data
    self.rideTime = 0;
}


#pragma mark - Public Methods
- (void)beginRecordRideInfo {
    self.rideTime = 0;
    self.rideTimeLabel.text = @"00:00:00";
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countRideTime) userInfo:nil repeats:YES];
}

#pragma mark - Private Methods
#pragma mark Views Behavior
- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self addSubview:self.rideTimeDesLabel];
    [self addSubview:self.rideTimeLabel];
    [self addSubview:self.rideCostLabel];
    [self addSubview:self.rideDistanceLabel];
    [self addSubview:self.finishRideButton];
    [self addSubview:self.rideSecurityLabel];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.rideTimeDesLabel.frame = CGRectMake(0, 20, self.width, 20);
    self.rideTimeLabel.frame = CGRectMake(0,
                                          CGRectGetMaxY(self.rideTimeDesLabel.frame),
                                          self.width,
                                          70);
    
    self.rideCostLabel.frame = CGRectMake(kCostLabelBoardLeft ,
                                          CGRectGetMaxY(self.rideTimeLabel.frame) + 5,
                                          self.width / 2.0 - kCostLabelBoardLeft,
                                          kCostLabelHeight);
    
    self.rideDistanceLabel.frame = CGRectMake(self.width / 2.0,
                                              self.rideCostLabel.y,
                                              self.rideCostLabel.width,
                                              self.rideCostLabel.height);
    
    self.finishRideButton.frame = CGRectMake((self.width - kFinishButtonWidth) / 2.0,
                                             CGRectGetMaxY(self.rideCostLabel.frame) + 10,
                                             kFinishButtonWidth,
                                             kFinishButtonHeight);
    
    self.finishRideButton.themeColorLayer.cornerRadius = kFinishButtonHeight / 2.0;
    
    self.rideSecurityLabel.frame = CGRectMake(0 ,
                                              self.height - kCostLabelHeight,
                                              self.width,
                                              kSecurityLabelHeight);
}

#pragma mark DataSource Control

#pragma mark - Event Action
- (void)finishRide {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - System Delegate


#pragma mark - Custom Delegate


#pragma mark - Util Method
- (void)countRideTime {
    self.rideTime++;
    NSString *timeString = [self getTimeString:self.rideTime];
    self.rideTimeLabel.text = timeString;
}

- (NSString *)getTimeString:(NSInteger)count {
    NSInteger second = count % 60;
    NSInteger minuteCount = (count - second) / 60;
    NSInteger minute = minuteCount % 60;
    NSInteger hour = minuteCount / 60;
    
    NSString *timeString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",hour ,minute ,second];
    return timeString;
}

- (NSAttributedString *)getCostAttribute:(CGFloat)cost {
    return [self prefixString:@"当前费用: " highLevelString:[NSString stringWithFormat:@"%.1f",cost] suffixString:@" 元"];
}

- (NSAttributedString *)getDistanceAttribute:(CGFloat)distance {
    return [self prefixString:@"已骑行: " highLevelString:[NSString stringWithFormat:@"%.1f",distance] suffixString:@" km"];
}


- (NSAttributedString *)prefixString:(NSString *)prefix highLevelString:(NSString *)highString suffixString:(NSString *)suffix {
    NSDictionary *normalAttribute = @{NSFontAttributeName : [UIFont systemFontOfSize:13] , NSForegroundColorAttributeName : [UIColor lightGrayColor]};
    NSDictionary *highAttributeDic = @{NSFontAttributeName : [UIFont fontWithName:@"DINAlternate-Bold" size:23], NSForegroundColorAttributeName : [UIColor blackColor]};
    
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc]initWithString:prefix attributes:normalAttribute];
    
    NSAttributedString *highAttribute= [[NSAttributedString alloc]initWithString:highString attributes:highAttributeDic];
    [attribute appendAttributedString:highAttribute];
    
    NSAttributedString *suffixAttribute = [[NSAttributedString alloc]initWithString:suffix attributes:normalAttribute];
    [attribute appendAttributedString:suffixAttribute];
    
    return attribute;
}

#pragma mark - Getters And Setters

- (UILabel *)rideTimeDesLabel {
    if(!_rideTimeDesLabel){
        _rideTimeDesLabel = [UILabel new];
        _rideTimeDesLabel.text = NSLocalizedString(@"骑行时间", nil);
        _rideTimeDesLabel.textColor = [UIColor lightGrayColor];
        _rideTimeDesLabel.textAlignment = NSTextAlignmentCenter;
        _rideTimeDesLabel.font = [UIFont systemFontOfSize:12];
    }
    return _rideTimeDesLabel;
}//骑行时间描述

- (UILabel *)rideTimeLabel {
    if(!_rideTimeLabel){
        _rideTimeLabel = [UILabel new];
        _rideTimeLabel.text = NSLocalizedString(@"00:00:00", nil);
        _rideTimeLabel.textColor = [UIColor blackColor];
        _rideTimeLabel.textAlignment = NSTextAlignmentCenter;
        _rideTimeLabel.font = [UIFont fontWithName:@"DINAlternate-Bold" size:60];
//        _rideTimeLabel.font = [UIFont boldSystemFontOfSize:40];
    }
    return _rideTimeLabel;
}//骑行时间

- (UILabel *)rideCostLabel {
    if(!_rideCostLabel){
        _rideCostLabel = [UILabel new];
        _rideCostLabel.attributedText = [self getCostAttribute:2.0];
        _rideCostLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _rideCostLabel;
}//骑行费用

- (UILabel *)rideDistanceLabel {
    if(!_rideDistanceLabel){
        _rideDistanceLabel = [UILabel new];
        _rideDistanceLabel.attributedText = [self getDistanceAttribute:16.5];
        _rideDistanceLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _rideDistanceLabel;
}//骑行距离

- (CBLThemeColorButton *)finishRideButton {
    if(!_finishRideButton){
        _finishRideButton = [CBLThemeColorButton new];
        _finishRideButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_finishRideButton setTitle:@"结束行程" forState:UIControlStateNormal];
        [_finishRideButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return _finishRideButton;
}//结束骑行

- (UILabel *)rideSecurityLabel {
    if(!_rideSecurityLabel){
        _rideSecurityLabel = [UILabel new];
        _rideSecurityLabel.text = NSLocalizedString(@"ofo已经为您本次出行购买了《安全保障》", nil);
        _rideSecurityLabel.textColor = [UIColor lightGrayColor];
        _rideSecurityLabel.textAlignment = NSTextAlignmentCenter;
        _rideSecurityLabel.font = [UIFont systemFontOfSize:12];
    }
    return _rideSecurityLabel;
}//骑行安全协议


@end
