//
//  CBLStartBottomView.h
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomBaseView.h"
#import "CBLThemeColorButton.h"

@interface CBLBottomStartView : CBLBottomBaseView

@property (nonatomic, strong) CBLThemeColorButton *startButton;

@property (nonatomic, strong) UIButton *userButton;

@property (nonatomic, strong) UIButton *giftButton;

@end
