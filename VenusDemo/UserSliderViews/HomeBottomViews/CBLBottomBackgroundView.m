//
//  CBLBottomBackgroundView.m
//  VenusDemo
//
//  Created by pf on 17/04/2017.
//  Copyright © 2017 北京拜克洛克科技有限公司. All rights reserved.
//

#import "CBLBottomBackgroundView.h"
#import "CBLBottomShapeLayer.h"

@interface CBLBottomBackgroundView ()



@end

@implementation CBLBottomBackgroundView

#pragma mark - Initialize

- (instancetype)initWithFirstView:(CBLBottomBaseView *)firstView {
    self = [super init];
    if(self){
        [self addSubview:firstView];
        self.currentView = firstView;
    }
    return self;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


#pragma mark - Views Behavior
- (void)switchToNextView:(CBLBottomBaseView *)view {
    self.currentView = view;
    [self animationToNextView:view];
}

- (void)switchToNextView:(CBLBottomBaseView *)view animation:(void (^)())animation {
    self.currentView = view;
    animation();
}

- (void)animationToNextView:(CBLBottomBaseView *)nextView {
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:5 initialSpringVelocity:5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        nextView.x = 0;
        self.lastView.x = -self.width;
    } completion:^(BOOL finished) {
        [self.lastView removeFromSuperview];
    }];
}

#pragma mark - Public Methods



#pragma mark - Private Methods
-(void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = CGRectMake(0, 0, self.width, self.height);
    self.currentView.frame = frame;
}


#pragma mark - Getters And Setters
- (void)setCurrentView:(UIView *)currentView {
    if (_currentView) {
        self.lastView = _currentView;
    }
    _currentView = currentView;
    _currentView.frame = CGRectMake(self.width, 0, self.width, self.height);
    [self addSubview:_currentView];
}

@end

